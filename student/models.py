import ondelete as ondelete
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from .managers import UserManager


# Create your models here.

class User(AbstractBaseUser, PermissionsMixin):
    """

    """
    GENDER_CHOICES = (
        ('M', 'Masculino'),
        ('F', 'Femenino'),
        ('O', 'Otro')

    )

    username = models.CharField(max_length=150, unique=True)
    email = models.EmailField()
    firstname = models.CharField(max_length=50, blank=True)
    lastname = models.CharField(max_length=50, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True)
    #
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', ]

    objects = UserManager()

    def get_short_name(self):
        """
        this function returns the short name of the User model
        """
        return self.firstname

    def get_full_name(self):
        """
        this fuction returns the full name of the User model
        """
        return self.firstname + ' ' + self.lastname


class Skills(models.Model):
    """
    Skills Model
    """

    skills = models.CharField('skills', max_length=250)

    class meta:
        verbose_name = 'skills'
        verbose_name_plural = 'skills student'

    def __str__(self):
        return str(self.id) + ' _ ' + self.skills


class Business(models.Model):
    """
    Business Model
    """
    name = models.CharField('name', max_length=250, unique=True)
    shor_name = models.CharField('shor name', max_length=140)

    class Meta:
        verbose_name = 'business'

        ordering = ['name']
        unique_together = ('name', 'shor_name')

    def __str__(self):
        return self.name


class Student(models.Model):
    """
    Student model
    """
    dominant_programming_language = (
        ('0', 'JavaScript-React'),
        ('1', 'Python-Django'),
        ('2', 'Odoo'),
        ('3', 'other')
    )

    firstname = models.CharField('firstname', max_length=250)
    lastname = models.CharField('lastname', max_length=250)
    dominant_language = models.CharField(max_length=1, choices=dominant_programming_language)
    skills = models.ManyToManyField(Skills, null=True, blank=True)
    business = models.ForeignKey(Business, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'student'
        verbose_name_plural = 'students'
        ordering = ['id']

    def __str__(self):
        return str(self.id) + ' _ ' + self.firstname + ' _ ' + self.lastname
