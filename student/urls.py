"""students URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from student.views import(
    HomeView,
    ListAllStudents,
    DetailStudent,
    RegisterStudent,
    UpdateStudent,
    DeleteStudent,
    CreateSkills,
    UserRegister,
    LoginUser,
    CreateBuiness
)

urlpatterns = [
    path('home/', HomeView.as_view(), name='home'),
    path('ListEstudents/', ListAllStudents.as_view(), name='ListAllStudents'),
    path('Detail-Student/<pk>/', DetailStudent.as_view(), name='DetailStudent'),
    path('RegisterStudent/', RegisterStudent.as_view(), name="RegisterStudent"),
    path('UpdtateStudent/<pk>/', UpdateStudent.as_view(), name="UpdateStudent"),
    path('DeleteStudent/<pk>', DeleteStudent.as_view(), name='DeleteStudent'),
    path('CreateSkill/', CreateSkills.as_view(), name="CreateSkill"),
    path('create_business/',CreateBuiness.as_view(), name='create_business'),
    path('', UserRegister.as_view(), name="RegisterUser"),
    path('Login/', LoginUser.as_view(), name='Login'),

]
