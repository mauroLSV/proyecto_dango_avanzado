from django.contrib import admin

# Register your models here.
from .models import Student, Skills, Business, User


class UserAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'email',
        'firstname',
        'lastname',
        'gender',
    )


admin.site.register(User, UserAdmin)


class SkillsAdmin(admin.ModelAdmin):
    list_display = (
        'skills',
    )
    search_fields = ('skills',)


admin.site.register(Skills, SkillsAdmin)


class BusinessAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'shor_name',
    )


admin.site.register(Business, BusinessAdmin)


class StudentAdmin(admin.ModelAdmin):
    list_display = (
        'firstname',
        'lastname',
        'dominant_language',
        'business',
        'full_name'

    )

    @staticmethod
    def full_name(obj):
        """
        this function assigns a value to FullName from the
        firstname and lastname stored in the student model.

        :params: obj: this argument fetches each item that is listed in the StudentAdmin class

        """

        return obj.firstname + ' ' + obj.lastname

    search_fields = ('id', 'firstname',)
    list_filter = ('id', 'firstname')
    filter_horizontal = ('skills',)


admin.site.register(Student, StudentAdmin)
