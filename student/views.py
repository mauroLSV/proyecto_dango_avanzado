from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login
from django.views.generic import (
    TemplateView,
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.views.generic.edit import FormView
from student.forms import FormStudents, FormSkills, UserRegisterForm, LoginForm, FormBusiness
from student.models import Student, Skills, User, Business


class UserRegister(FormView):
    template_name = 'RegisterUser.html'
    form_class = UserRegisterForm
    success_url = reverse_lazy('Login')

    def form_valid(self, form):
        User.objects.create_user(
            form.cleaned_data['username'],
            form.cleaned_data['email'],
            form.cleaned_data['password1'],
            firstname=form.cleaned_data['firstname'],
            lastname=form.cleaned_data['lastname'],
            gender=form.cleaned_data['gender'],

        )

        return super(UserRegister, self).form_valid(form)


class LoginUser(FormView):
    template_name = 'Login.html'
    form_class = LoginForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        user = authenticate(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password'],
        )
        login(self.request, user)
        return super(LoginUser, self).form_valid(form)


class HomeView(TemplateView):
    """
    this view returns the start template
    """
    template_name = 'home.html'


class ListAllStudents(ListView):
    """
    Student list by keyword
    """
    template_name = 'ListStudents.html'
    paginate_by = 5
    ordering = 'id'

    def get_queryset(self):
        keyword = self.request.GET.get('word', '')
        list = Student.objects.filter(firstname__icontains=keyword)
        return list


class DetailStudent(DetailView):
    """

    """
    model = Student
    template_name = 'DetailStudent.html'


class RegisterStudent(CreateView):
    template_name = 'RegisterStudent.html'
    model = Student
    form_class = FormStudents
    success_url = reverse_lazy('ListAllStudents')

    def form_valid(self, form):
        student = form.save(commit=False)  # para evitar guardar dos veces en la base de datos
        student.fullName = student.firstname + student.lastname
        student.save()
        return super(RegisterStudent, self).form_valid(form)


class UpdateStudent(UpdateView):
    model = Student
    template_name = 'UpdateStudents.html'
    fields = ['id', 'firstname', 'lastname', 'dominant_language', 'skills', 'business']
    success_url = reverse_lazy('ListAllStudents')

    def post(self, request, *args, **kwargs):
        print('====METODO POST======')
        print(request.POST)
        print(request.POST['lastname'])
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        print('*****METODO FORM VALID*********')
        print(form)
        return super().form_valid(form)


class DeleteStudent(DeleteView):
    model = Student
    template_name = 'DeleteStudent.html'
    success_url = reverse_lazy('ListAllStudents')


class CreateSkills(CreateView):
    template_name = 'CreateSkills.html'
    model = Skills
    form_class = FormSkills
    success_url = reverse_lazy('CreateSkill')


class CreateBuiness(CreateView):
    template_name = 'CreateBusiness.html'
    model = Business
    form_class = FormBusiness
    success_url = reverse_lazy('home')


class ListAllSkills(ListView):
    """

    """
    template_name = 'CreateSkills.html'
    paginate_by = 5
    ordering = 'id'
