from .models import Student, Skills, User, Business
from django import forms


class UserRegisterForm(forms.ModelForm):
    password1 = forms.CharField(
        label='password',
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Password'
            }
        )
    )
    password2 = forms.CharField(
        label='password',
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Repeat password'
            }
        )
    )

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'firstname',
            'lastname',
            'gender',
        )

    def clean_password2(self):
        """
        this function validates that the passwords are not different
        """
        if self.cleaned_data['password1'] != self.cleaned_data['password2']:
            self.add_error("password2", "Passwords do not match")
        # print(len(self.cleaned_data['password1']))

    # def clean_password1(self):
    #     if len(self.cleaned_data['password1']) < 5:
    #         self.add_error("password1", "error, password is too short")


class LoginForm(forms.Form):
    username = forms.CharField(
        label='username',
        required=True,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'username',
                'style': '{margin: 10px}'
            }
        )
    )

    password = forms.CharField(
        label='password',
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Password'
            }
        )
    )


class FormStudents(forms.ModelForm):
    class Meta:
        model = Student
        fields = (
            'firstname',
            'lastname',
            'dominant_language',
            'skills',
            'business',
        )

        widgets = {
            'skills': forms.CheckboxSelectMultiple()
        }


class FormSkills(forms.ModelForm):
    class Meta:
        model = Skills
        fields = (
            'skills',
        )


class FormBusiness(forms.ModelForm):
    class Meta:
        model = Business
        fields = (
            'name',
            'shor_name'
        )
